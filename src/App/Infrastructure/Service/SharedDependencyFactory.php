<?php

namespace App\Infrastructure\Service;


use App\Application\Service\LoginUserUseCase\LoginUserUseCase;
use App\Application\Service\RegisterUserUseCase\RegisterUserUseCase;
use App\Infrastructure\Repository\MySQLUserRepository;
use App\Infrastructure\Service\Crypt\BuiltInHasher;
use App\Infrastructure\Service\Crypt\CustomHasher;
use PDO;
use Pimple;

class SharedDependencyFactory
{

    public static function addSharedDependencies(Pimple $container)
    {

        $container["user_password_salt"] = "ilovecodeofaninjabymikedalisay";

        $container["mysql_configuration"] = function () {
            return [
                "host" => "127.0.0.1",
                "dbname" => "coan_secure",
                "user" => "root",
                "password" => "vagrantpass"
            ];
        };

        $container["mysql_connector"] = function (Pimple $container) {
            $config = $container["mysql_configuration"];

            return new PDO("mysql:host=${config["host"]};dbname=${config["dbname"]}", $config["user"], $config["password"]);
        };

        $container["user_repository"] = function (Pimple $container) {
            $containeronnector = $container["mysql_connector"];
            return new MySQLUserRepository($containeronnector);
        };

        $container["user_password_hasher"] = function (Pimple $container) {
            return CRYPT_BLOWFISH ? new BuiltInHasher($container["user_password_salt"]) : new CustomHasher($container["user_password_salt"]);
        };

        $container["register_use_case"] = function (Pimple $container) {
            $userRepository = $container["user_repository"];
            $hasher = $container["user_password_hasher"];
            return new RegisterUserUseCase($userRepository, $hasher);
        };

        $container["login_use_case"] = function (Pimple $container) {
            $userRepository = $container["user_repository"];
            $hasher = $container["user_password_hasher"];
            return new LoginUserUseCase($userRepository, $hasher);
        };

        return $container;
    }

    public static function createContainerWithSharedDependencies()
    {
        $container = new Pimple();
        return self::addSharedDependencies($container);
    }

}