<?php

namespace App\Infrastructure\Service\Crypt;

include_once __DIR__."/../Crypt/../../../../libs/PasswordHash.php";

use App\Domain\Service\Crypt\Hasher;

class CustomHasher implements Hasher
{

    private $hashBackend;

    private $salt;

    public function __construct($salt)
    {
        $this->salt = $salt;
        $this->hashBackend = new \PasswordHash(8, false);
    }

    public function hash($password)
    {
        return $this->hashBackend->HashPassword($this->salt.$password);
    }


    public function verify($password, $hash)
    {
        return $this->hashBackend->CheckPassword($this->salt.$password, $hash);
    }
}