<?php

namespace App\Infrastructure\Service\Crypt;

use App\Domain\Service\Crypt\Hasher;

class BuiltInHasher implements Hasher
{

    private $salt;

    public function __construct($salt)
    {
        $this->salt = $salt;
    }

    public function hash($password)
    {
        return password_hash($this->salt.$password, CRYPT_BLOWFISH);
    }


    public function verify($password, $hash)
    {
        return password_verify($this->salt.$password, $hash);
    }
}