<?php

namespace App\Infrastructure\Cli\Command;


use App\Application\Service\LoginUserUseCase\LoginUserRequest;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoginUserCommand extends Command
{

    private $container;

    public function __construct($container, $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName("user:login")
            ->setDescription("Login a user")
            ->addArgument(
                "email"
                , InputArgument::REQUIRED
                , "The user email")
            ->addArgument(
                "password"
                , InputArgument::REQUIRED
                , "The user password"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->container;
        $email = $input->getArgument("email");
        $password = $input->getArgument("password");
        $loginUserUseCase = $container["login_use_case"];
        $isUserRegistered = $loginUserUseCase->execute(
            new LoginUserRequest(
                $email,
                $password
            ));
        if (!$isUserRegistered) {
            $output->writeln("Access denied.");
            return null;
        }
        $output->writeln("Access granted.");
    }
}