<?php

namespace App\Infrastructure\Cli\Command;


use App\Application\Service\RegisterUserUseCase\RegisterUserRequest;
use App\Domain\Repository\RepositoryException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterUserCommand extends Command
{

    private $container;

    public function __construct($container, $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName("user:register")
            ->setDescription("Register a new user")
            ->addArgument(
                "email"
                , InputArgument::REQUIRED
                , "The user email")
            ->addArgument(
                "password"
                , InputArgument::REQUIRED
                , "The user password"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->container;
        $email = $input->getArgument("email");
        $password = $input->getArgument("password");
        $registerUserUseCase = $container["register_use_case"];
        $response = $registerUserUseCase->execute(
            new RegisterUserRequest(
                $email,
                $password
            ));
        if (!$response) {
            $output->writeln("Unable to register.");
            return null;
        }
        $output->writeln("Successful registration.");
    }

}