<?php

namespace App\Domain\Service\Crypt;


interface Hasher
{

    public function hash($password);

    public function verify($password, $hash);

}