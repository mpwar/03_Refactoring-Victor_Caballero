<?php

namespace App\Application\Service\RegisterUserUseCase;

use App\Domain\Repository\RepositoryException;
use App\Domain\Repository\SaveUserRequest;
use App\Domain\Repository\UserRepository;
use App\Domain\Service\Crypt\Hasher;

class RegisterUserUseCase
{

    private $userRepository;

    private $hasher;

    public function __construct(UserRepository $userRepository, Hasher $hasher)
    {
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
    }

    public function execute(RegisterUserRequest $request)
    {
        $hashedPassword = $this->hasher->hash($request->getPassword());

        $saveUserRequest = new SaveUserRequest($request->getEmail(), $hashedPassword);
        try {
            $this->userRepository->saveUser($saveUserRequest);
            return true;
        } catch (RepositoryException $re) {
            return false;
        }
    }
}