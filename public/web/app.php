<?php

date_default_timezone_set('Europe/Madrid');

error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once(__DIR__ . "/../../vendor/autoload.php");

use App\Infrastructure\Http\Controller\LoginUserController;
use App\Infrastructure\Http\Controller\RegisterUserController;
use App\Infrastructure\Service\SharedDependencyFactory;

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../../resources/views',
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app = SharedDependencyFactory::addSharedDependencies($app);

$app->mount("/register", new RegisterUserController());

$app->mount("/login", new LoginUserController());


$app->run();

