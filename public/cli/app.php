<?php

use App\Infrastructure\Cli\Command\LoginUserCommand;
use App\Infrastructure\Cli\Command\RegisterUserCommand;
use App\Infrastructure\Service\SharedDependencyFactory;
use Symfony\Component\Console\Application;

date_default_timezone_set('Europe/Madrid');

error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once(__DIR__ . "/../../vendor/autoload.php");

$c = new Pimple();

$c = SharedDependencyFactory::addSharedDependencies($c);

$application = new Application();
$application->add(new LoginUserCommand($c));
$application->add(new RegisterUserCommand($c));

$application->run();