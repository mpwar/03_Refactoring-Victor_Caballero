# Ejercicio Refactoring

## Enunciado

Debemos refactorizar una aplicación _legacy_ que tiene las siguientes funcionalidades: registrar usuarios y permitirles hacer login. Además deberemos permitir ejecutar estas funcionalidades por consola.

## Code smells

* La lógica de negocio, la capa de persistencia y la capa de presentación están muy acopladas. Además de estar en el mismo fichero, sentencias que atacan a diferentes capas de la aplicación están mezcladas y cuesta de leer. Evidencia: Solo existen dos ficheros, uno para registrar usuarios y otro para que hagan login.
* Constantes mágicas repetidas en los dos ficheros: el _salt_. Evidencia:
 
	```php 
	 $salt = "ilovecodeofaninjabymikedalisay";
	```
	
* Acomplamiento al protocolo HTTP en diferentes puntos de la aplicación. Evidencia:

	```php
		if($_POST){ ...
		$_POST['email'] ...
	```

* El código no es semántico. Las diferentes funcionalidades podrían estar almenos agrupadas en funciones con un nombre descriptivo.

En definitiva, el código está muy mal estructurado.


## Refactorización y re-organicación de código

Con motivo de poder aprovechar los conceptos aprendidos en clase y recibir un _feedback_ sobre éstos, he decidido aplicar arquitectura hexagonal. No obstante, es evidente que para un proyecto de estas características (registrar y loguear usuarios) no hace falta y se peca de sobre-ingeniería.

El primer paso es tener claro que las distintas capas: presentación, infraestructura y lógica de negocio deben estar separadas. Se debe eliminar el acomplamiento presente en el código original.

Es por este motivo que he decidido usar _Silex_ como framework para la infraestructura web, es decir, la infraestructura que recibe y responde al protocolo HTTP es Silex. Además, para generar las respuestas en HTML y separar la lógica de la vista he usado _twig_.

```php
$app = new Silex\Application();
$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../../resources/views',
));
```

Por otro lado, para los comandos he usado el componente de consola de symfony.

```php
$application = new Application();
$application->add(new UserLoginCommand($c));
$application->add(new UserRegisterCommand($c));
```

Debido a que la lógica es común sea cual sea el punto de entrada a la aplicación (web o consola), la lógica se ha agrupado en casos de uso, por ejemplo:

```php
class LoginUserUseCase
{
    private $userRepository;

    private $hasher;

    public function __construct(UserRepository $userRepository, Hasher $hasher)
    {
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
    }

    public function execute(LoginUserRequest $request)
    {
        try {

            $user = $this->userRepository->getUserByEmail($request->getEmail());
            if (!isset($user)) {
                return false;
            }
            $isUserRegistered = $this->verifyPassword($request->getPassword(), $user->getPassword());

            if (!$isUserRegistered) return false;

            return $user;

        } catch (RepositoryException $re) {
            return false;
        }

    }

    private function verifyPassword($password, $hash) {
        return $this->hasher->verify($password, $hash);
    }

}
```

Tal y como se puede observar, la lógica para loguear a un usuario se encuentra en esta clase ```LoginUserUseCase```, la cual es usada tanto por el controlador web como por el comando para loguear a un usuario. Esta clase recibe un DTO para con los datos que necesita para ejecutarse.

Controlador:

```php
class LoginUserController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $userLoginControllers = $app["controllers_factory"];

        $userLoginControllers->get("/", array($this, "login"))->bind("login");
        $userLoginControllers->post("/", array($this, "loginSubmit"))->bind("login_submit");

        return $userLoginControllers;
    }

    public function login(Application $app)
    {
        return $app['twig']->render('login.twig');
    }

    public function loginSubmit(Application $app, Request $request)
    {
        $loginUserUseCase = $app["login_use_case"];
        $isUserRegistered = $loginUserUseCase->execute(
            new LoginUserRequest(
                $request->request->get("email"),
                $request->request->get("password")
            ));
        if (!$isUserRegistered) {
            return $app['twig']->render('login_failure_response.twig');
        }
        return $app['twig']->render('login_success_response.twig');

    }
}
```


Comando:

```php
class LoginUserCommand extends Command
{

    private $container;

    public function __construct($container, $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName("user:login")
            ->setDescription("Login a user")
            ->addArgument(
                "email"
                , InputArgument::REQUIRED
                , "The user email")
            ->addArgument(
                "password"
                , InputArgument::REQUIRED
                , "The user password"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->container;
        $email = $input->getArgument("email");
        $password = $input->getArgument("password");
        $loginUserUseCase = $container["login_use_case"];
        $isUserRegistered = $loginUserUseCase->execute(
            new LoginUserRequest(
                $email,
                $password
            ));
        if (!$isUserRegistered) {
            $output->writeln("Access denied.");
            return null;
        }
        $output->writeln("Access granted.");
    }
}
```

Se puede observar también que el caso "correcto" está en el primer nivel de identación. ```Access granted``` está en primer nivel y ```Access denied.``` está en segundo nivel.

Entiendo que debe haber un poco de duplicidad de código en algún punto de la aplicación, sobretodo en la infraestructura responsable de recibir el input y generar el output correspondiente.

En la clase ```LoginUserUseCase``` podemos observar también el uso de los tipos ```Hasher``` y ```UserRepository```. Estos tipos corresponden a dos interfaces definidas en el dominio de la aplicación e implementadas en su infraestructura. En el caso de ```Hasher``` podemos ver perfectamente el uso de __Dependency Inversion Principle__ ya que dos clases implementan esta interfaz: ```BuiltInHasher``` y ```CustomHasher``` que responden a la necesidad de poder usar dos maneras de hashear una contraseña con el mismo método. Dependiendo de la versión de PHP (o un flag), se usará una clase u otra.

```php
$c["user_password_hasher"] = function (Pimple $c) {
	return CRYPT_BLOWFISH ? new BuiltInHasher($c["user_password_salt"]) : new CustomHasher($c["user_password_salt"]);
};

$c["login_use_case"] = function (Pimple $c) {
	$userRepository = $c["user_repository"];
	$hasher = $c["user_password_hasher"];
	return new LoginUserUseCase($userRepository, $hasher);
};
```

La interfaz ```UserRepository``` es implementada por ```MySQLUserRepository``` en la que podemos ver el uso de constantes en vez de cadenas para referirnos a los campos MySQL de la tabla de la base de datos (no magic constants):

```php
const USER_ID_KEY = "id";
const USER_EMAIL_KEY = "email";
const USER_PASSWORD_KEY = "password";
```

Y la instanciación del connector a la base de datos fuera del repositorio y a partir de una configuración:

```php
public function __construct($conn)
{
	$this->connector = $conn;
}

....

$container["mysql_configuration"] = function () {
    return [
        "host" => "127.0.0.1",
        "dbname" => "coan_secure",
        "user" => "root",
        "password" => "vagrantpass"
    ];
};

$container["mysql_connector"] = function (Pimple $container) {
    $config = $container["mysql_configuration"];

    return new PDO("mysql:host=${config["host"]};dbname=${config["dbname"]}", $config["user"], $config["password"]);
};
```

Durante esta explicación, se ha podido observar el uso de un inyector de dependencias, concretamente _Pimple_ para construir los diferentes objetos de la aplicación.

Debido a que tanto la consola como la web comparten la necesidad de los mismos componentes (concretamente casos de uso), he intentado unificar de alguna forma la configuración del inyector de dependencias con el motivo de no tener que duplicar código en ambos puntos de entrada.
La mejor manera que se me ha ocurrido de unificar esta configuración ha sido la creación de una función que, dado un inyector de dependencias del tipo _Pimple_, lo configura y lo devuelve.

```php
public static function addSharedDependencies(Pimple $container)
{

    $container["user_password_salt"] = "ilovecodeofaninjabymikedalisay";

    $container["mysql_configuration"] = function () {
        return [
            "host" => "127.0.0.1",
            "dbname" => "coan_secure",
            "user" => "root",
            "password" => "vagrantpass"
        ];
    };

    $container["mysql_connector"] = function (Pimple $container) {
        $config = $container["mysql_configuration"];

        return new PDO("mysql:host=${config["host"]};dbname=${config["dbname"]}", $config["user"], $config["password"]);
    };

    $container["user_repository"] = function (Pimple $container) {
        $containeronnector = $container["mysql_connector"];
        return new MySQLUserRepository($containeronnector);
    };

    $container["user_password_hasher"] = function (Pimple $container) {
        return CRYPT_BLOWFISH ? new BuiltInHasher($container["user_password_salt"]) : new CustomHasher($container["user_password_salt"]);
    };

    $container["register_use_case"] = function (Pimple $container) {
        $userRepository = $container["user_repository"];
        $hasher = $container["user_password_hasher"];
        return new RegisterUserUseCase($userRepository, $hasher);
    };

    $container["login_use_case"] = function (Pimple $container) {
        $userRepository = $container["user_repository"];
        $hasher = $container["user_password_hasher"];
        return new LoginUserUseCase($userRepository, $hasher);
    };

    return $container;
}
```

No obstante, el hecho de modificar una variable en una función no me acaba de gustar (side-effects). Otro modo sería crear en cada punto de entrada de la aplicación un DIC e instanciar otro con las configuraciones predefinidias e ir igualando claves en el "array":
 
```php
$container["mysql_connector"] = $sharedContainer["mysql_connector"]
```
Otra opción sería duplicar la configuración del DIC en cada punto de entrada de la aplicación, esto haria el código más explícito. _¿Qué piensas Javi?_

Tal y como se ha podido ver, durante la refactorización del código he tenido muy presente el __Single Responsibility Principle__. Cada clase tiene una única responsabilidad y la composición entre ellas resulta en las funcionalidades de la aplicación deseadas. Los nombres de las variables y los métodos también són __semánticos__.

## Para poner en marcha la aplicación

* Ejecutar ```composer install```
* Crear la base de datos, la configuración de ésta está en ```App/Infrastructure/Service/SharedDependencyFactory.php```
* Los puntos de entrada tanto para la aplicación web como para la aplicación cli están el la carpeta public -> ```web/app.php``` y ```cli/app.php``` respectivamente.
	* Para entrar en la web, se debe entrar con el navegador al direco¡torio mencionado anteriormente y añadir ```/login``` o ```/register```
	* Para usar la aplicación cli ejecutar php app.php ```user:register <email> <password>``` y/o ```user:login <email> <password>```

